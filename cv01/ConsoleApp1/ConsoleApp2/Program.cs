﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Jak se jmenujes?");
            string jmeno = Console.ReadLine();
            Console.WriteLine("Kolikatym studujes rokem?");
            string koliks = Console.ReadLine();
            //int koliki = Convert.ToInt32(koliks);
            int koliki = 0;
            if (int.TryParse(koliks, out koliki))
            {
                Console.WriteLine("Ahoj " + jmeno + " :-) Teprve " + koliki.ToString());
            }
            else
            {
                Console.WriteLine("To nebylo cislo!");
            }
            
            Console.ReadLine();
        }
    }
}
