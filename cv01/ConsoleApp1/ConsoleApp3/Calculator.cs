﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Vypocty
{
    public class Calculator
    {
        private Single operand1;
        private Single operand2;
        private double vysledek = 3;

        public Single Operand1
        {
            get {
                return operand1; 
            }
            set {
                operand1 = value;
            }
        }

        public Single Operand2
        {
            get
            {
                return operand2;
            }
            set
            {
                operand2 = value;
            }
        }

        public double Vysledek
        {
            get 
            {
                return vysledek;
            }
        }

        public string VysledekAsStr
        {
            get
            {
                string s = string.Format("Vysledek je {0}.", vysledek);
                return s;
            }
        }

        public void Secti()
        {
            vysledek = operand1 + operand2;
        }

    }
}
