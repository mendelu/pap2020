﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seznam
{
    public class Polozka
    {
        private int data;
        private Polozka next;
        private Polozka prev;

        public Polozka Next
        {
            get { return next; }
        }

        public Polozka Prev
        {
            get { return prev; }
        }

        public int Data
        {
            get {
                return data; 
            }
            set {
                data = value; 
            }
        }

        public string GetCaption()
        {
            return data.ToString();
        }

        public Polozka AddNext(int newData)
        {
            Polozka newItem = new Polozka();

            if (this.next != null)
            {
                this.next.prev = newItem;
            }
            newItem.data = newData;
            newItem.next = this.next;
            newItem.prev = this;
            this.next = newItem;

            return newItem;
        }

        public bool RemoveNext()
        {
            if (next == null) return false;
            next = next.next;
            if (next != null) { next.prev = this; }
            return true;
        }

        public void Vypis()
        {
            Console.WriteLine( GetCaption() );
            if (next != null) { next.Vypis(); }
            else { Console.WriteLine("konec seznamu"); }
        }
    }
}
