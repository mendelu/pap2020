﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seznam
{
    class Program
    {
        static void Main(string[] args)
        {
            Polozka magicStick;
            Polozka p1 = new Polozka();
            p1.Data = 1;
            magicStick = p1.AddNext(2);
            magicStick = magicStick.AddNext(3);

            Console.WriteLine( magicStick.RemoveNext());
            p1.Vypis();
            //Console.WriteLine(p1.GetCaption());
            Console.ReadLine();
        }
    }
}
