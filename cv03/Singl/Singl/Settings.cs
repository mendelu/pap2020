﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singl
{
    internal class Settings
    {
        private string path;

        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        private readonly double dataVersion;

        public double DataVersion
        {
            get { return dataVersion; }
        }

        private const int items = 5;

        public int Items
        {
            get { return items; }
        }

        public Settings()
        {
            dataVersion = 0.2;
            path = "C:\\";
        }

    }
}
