﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singl
{
    class SingletonSettings
    {
        private static readonly Settings instance = new Settings();

        private SingletonSettings() { }

        public static Settings Instance
        {
            get
            {
                return instance;
            }
        }
    }
}
