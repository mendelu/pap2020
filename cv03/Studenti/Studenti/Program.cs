﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studenti
{
    class Program
    {
        static void Main(string[] args)
        {
            Student s1 = new StudentCz("Alan", 200);
            Student s2 = new StudentSK("Bara", 1.1);
            s1.GetPopis();
            s2.CountScholarship();
            Console.ReadLine();
        }
    }
}
