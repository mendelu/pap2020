﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studenti
{
    abstract class Student
    {
        protected byte delkaStudia;
        private string name;
        private int id;
        private static int lastId;

        static Student()
        {
            lastId = 0;
        }

        private static int getNewId()
        {
            lastId++;
            return lastId;
        }

        public Student(string name)
        {
            this.id = Student.getNewId();
            this.name = name;
        }

        public virtual void GetPopis()
        {
            Console.WriteLine("-----------Student");
            Console.WriteLine(name);
            Console.WriteLine(id);
            Console.WriteLine(delkaStudia);
        }

        public abstract double CountScholarship();
        
    }
}
