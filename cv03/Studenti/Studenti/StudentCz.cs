﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studenti
{
    class StudentCz : Student
    {
        private double dojezdovaVzdalenost;

        public StudentCz(string ceskeJmeno):base(ceskeJmeno)
        {
            dojezdovaVzdalenost = 0;
        }

        public StudentCz(string ceskeJmeno, double dojezd) : base(ceskeJmeno)
        {
            dojezdovaVzdalenost = dojezd;
        }

        public override void GetPopis()
        {
            base.GetPopis();
            Console.WriteLine("Cz");
            Console.WriteLine("dojezd", dojezdovaVzdalenost);
        }

        public override double CountScholarship()
        {
            return (delkaStudia > 6) ? 0 : ((delkaStudia - 6)*5000);
        }
    }
}
