﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studenti
{
    class StudentSK : Student
    {
        private double studijniPrumer;

        public StudentSK(string ceskeJmeno) : base(ceskeJmeno)
        {
            studijniPrumer = 0;
        }

        public StudentSK(string ceskeJmeno, double prumer) : base(ceskeJmeno)
        {
            studijniPrumer = prumer;
        }

        public new void GetPopis()
        {
            base.GetPopis();
            Console.WriteLine("Sk");
            Console.WriteLine("prumer", studijniPrumer);
        }

        public override double CountScholarship()
        {
            double stipko = 0;
            if (delkaStudia > 6)
            {
                stipko = (delkaStudia - 6) * 5000;
            }
            if (studijniPrumer > 1.5)
            {
                stipko += delkaStudia * 500;
            }
            return stipko;
        }
    }
}
