﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace seznam
{
    public class Polozka<T> where T:IDataPolozky, IEquatable<T>
    {
        private T data;
        private Polozka<T> next;
        private Polozka<T> prev;

        public Polozka<T> Next
        {
            get { return next; }
        }

        public Polozka<T> Prev
        {
            get { return prev; }
        }

        public T Data
        {
            get {
                return data; 
            }
            set {
                data = value; 
            }
        }

        public string GetCaption()
        {
            return data.GetCaption();
        }

        public Polozka<T> AddNext(T newData)
        {
            Polozka<T> newItem = new Polozka<T>();

            if (this.next != null)
            {
                this.next.prev = newItem;
            }
            newItem.data = newData;
            newItem.next = this.next;
            newItem.prev = this;
            this.next = newItem;

            return newItem;
        }

        public bool RemoveNext()
        {
            if (next == null) return false;
            next = next.next;
            if (next != null) { next.prev = this; }
            return true;
        }

        public void Vypis()
        {
            Console.WriteLine( GetCaption() );
            if (next != null) { next.Vypis(); }
            else { Console.WriteLine("konec seznamu"); }
        }

        public bool NajdiData(T dataToFind, out int id)
        {
            id = 0;
            if (this.data.Equals(dataToFind))
            {
                return true;
            }
            else if (this.next != null)
            {
                return next.NajdiData(dataToFind, out id);
            }
            else
            {
                return false;
            }
        }

        
    }
}
