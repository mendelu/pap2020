﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Studenti;

namespace seznam
{
    class Program
    {
        static void Main(string[] args)
        {
            Polozka<Student> magicStick;
            Polozka<Student> p1 = new Polozka<Student>();
            p1.Data = new Student("Anna");
            magicStick = p1.AddNext(new Student("Bedrich"));
            magicStick = magicStick.AddNext(new Student("Cyril"));

            int foundId;
            p1.NajdiData(new Student("Cyril"), out foundId);



            Console.WriteLine( magicStick.RemoveNext());
            p1.Vypis();
            //Console.WriteLine(p1.GetCaption());
            Console.ReadLine();
        }
    }
}
