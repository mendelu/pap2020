﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silnice
{
    public enum TypeOfRoad { nespecifikovano, dalnice, tridaI, tridaII }

    public class Road
    {
        static int lastId = 0;
        private int id;
        private double km = 0;
        private TypeOfRoad roadType;

        public int ID { get { return id; } }

        public Road(TypeOfRoad roadType, double length)
        {
            id = lastId++;
            km = length;
            this.roadType = roadType;
        }

        public override string ToString()
        {
            return String.Format("road id {0} roadType {1} length {2}", id, roadType, km);
        }
    }

}
