﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silnice
{
    class RoadsManager
    {
        private Road[] roads;

        public RoadsManager(int maximalNumberOfRoads)
        {
            roads = new Road[maximalNumberOfRoads];
        }

        public void PrintRoads()
        {
            Console.WriteLine("-----items of roads manager:");
            foreach (Road r in roads)
            {
                if (r == null) continue;
                Console.WriteLine(r.ToString());
            }
            Console.WriteLine("-----");
        }

        public bool AddRoad(TypeOfRoad roadType, double length)
        {
            Road newRoad = new Road(roadType, length);
            for (int i = 0; i < roads.Length; i++)
            {
                if (roads[i] == null)
                {
                    roads[i] = newRoad;
                    return true;
                }
            }
            return false;
        }

        public bool RemoveAt(int index)
        {
            if (index >= roads.Length)
            {
                Console.WriteLine("nevhodny index");
                return false;
            }
            else if (roads[index] != null)
            {
                roads[index] = null;
                return true;
            }
            Console.WriteLine("neni co mazat");
            return false;
        }

        public Road this[int id]
        {
            get {
                foreach (Road r in roads)
                {
                    if (r == null) { continue; }
                    if (r.ID == id) return r;
                }
                return null;
            }
        }

    }
}
