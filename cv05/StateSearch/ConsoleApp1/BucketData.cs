﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class BucketData : ISpaceSearch<BucketData>, IEquatable<BucketData>
    {
        private TBuckets buckets;

        public byte RulesCount
        {
            get { return 6; }
        }

        private BucketData ancestor;
        public BucketData Ancestor
        {
            get { return ancestor; }
        }

        public bool Equals(BucketData other)
        {
            return buckets.Equals(other.buckets);
        }

        public int GetKey()
        {
            string id = buckets.Buck1.ToString() + buckets.Buck2.ToString();
            int result = 0;
            if (int.TryParse(id, out result))
            {
                return result;
            }
            else return -1;
        }

        public BucketData ApplyRule(byte ruleNo)
        {
            TBuckets newBuckets = new TBuckets(this.buckets.Buck1, this.buckets.Buck2);
            bool applicability = false;
            switch (ruleNo)
            {
                case 1:
                    applicability = newBuckets.NaplnPrvni();
                    break;
                //case 2:
                //
            }

            if (applicability == true)
            {
                BucketData newData = new BucketData(newBuckets);
                newData.ancestor = this;
                return newData;
            }
            else
            {
                return null;
            }

        }

        public BucketData(int buck1, int buck2)
        {
            buckets = new TBuckets(buck1, buck2);
        }

        public BucketData(TBuckets newBuckets)
        {
            buckets = newBuckets;
        }

        public override string ToString()
        {
            return string.Format("[{0},{1}]", buckets.Buck1, buckets.Buck2);
        }

    }
}
