﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    interface ISpaceSearch<T>
    {
        int GetKey();
        byte RulesCount { get; }
        T ApplyRule(byte ruleNo);

        T Ancestor { get; }
    }
}
