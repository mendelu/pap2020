﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Solver<T> where T: ISpaceSearch<T>, IEquatable<T>
    {
        SortedDictionary<int, T> close;
        Stack<T> open;
        List<T> solution;

        public Solver()
        {
            close = new SortedDictionary<int, T>();
            open = new Stack<T>();
            solution = new List<T>();
        }

        private bool SearchingKernel(T startState, T goalState)
        {
            bool found = false;
            open.Push(startState);
            T expandingState;
            // pridej do open pocatecnistav

            T newState;
            do
            {
                expandingState = open.Pop();
                // expandingState = stav z open
                close.Add(expandingState.GetKey(), expandingState);
                // pridej do close expandingstate

                for (byte i = 1; i <= expandingState.RulesCount; i++)
                {
                    newState = expandingState.ApplyRule(i);
                    if (newState == null) continue;
                    if (newState.Equals(goalState))
                    {
                        solution = new List<T>();
                        solution.Add(goalState);
                        found = true;
                        break;
                    }
                    if ((close.ContainsKey(newState.GetKey()) == false)&&(open.Contains(newState)==false))
                    {
                        open.Push(newState);
                    }
                }
                //for i = pravidlo
                // newstate =  expandigstate a dane pravidlo
                // neni newstate cilovy
                // zkontroluj zda newstate neni v close ani open, potom pridej do open
                //dokud jsme nenasli cilovy stav | open je prazdny
            }
            while ((found == false) && (open.Count > 0));
            return found;
        }

        private void BuildPath()
        {
            T actualState = solution[0];
            while (actualState.Ancestor != null)
            {
                actualState = actualState.Ancestor;
                solution.Insert(0, actualState);
            }

            foreach (T state in solution)
            {
                Console.WriteLine(state);
            }
        }

        public bool DepthSearch(T startState, T goalState)
        {
            bool found = SearchingKernel(startState, goalState);
            if (found)
            {
                BuildPath();
            }
            return found;
        }

    }
}
