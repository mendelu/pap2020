﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    struct TBuckets : IEquatable<TBuckets>
    {
        public int Buck1;
        public int Buck2;
        public const byte maxVolume1 = 5;
        public const byte maxVolume2 = 3;

        public TBuckets(int volume1, int volume2)
        {
            Buck1 = volume1;
            Buck2 = volume2;
        }

        public bool Equals(TBuckets other)
        {
            return ((Buck1 == other.Buck1) && (Buck2 == other.Buck2));
        }

        public bool NaplnPrvni()
        {
            if (Buck1 == maxVolume1) { return false; }
            Buck1 = maxVolume1;
            return true;
        }
    }
}
