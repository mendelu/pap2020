﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Majetek
    {
        List<Part> parts = new List<Part>();
        int porizovaciCena;


        public Majetek()
        {
            Random r = new Random();
            porizovaciCena = r.Next(-10, 10);
        }

        public void AddParts(int[] ceny)
        {
            foreach (int c in ceny)
            {
                parts.Add(new Part(c));
            }            
        }

        public double GetZbytkovaHodnota()
        {
            double pomZb = 0;
            List<int> problematicindexes = new List<int>();
            for (int i=0; i<parts.Count; i++)            
            {
                try
                {
                    pomZb += parts[i].GetZbytkovaCena();
                }
                catch (ExceptionNegativePrice e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Cast majetku ma zadanu spatnou porizovaci cenu, tuto cast odstranuji.");
                    problematicindexes.Add(i); //ma smysl jen pokud neposleme na nasledujicim radku vyjimku do vyssi metody pomoci throw ani nevyvolame jinou novou vyjimku
                    throw;
                }
                finally
                {
                    Console.WriteLine("Dalsi PART byla zpracována.");
                }
            }

            //pokud v catch teto metody nepredam vyjimku ani nevyhodim novou, probehne i nasledujici cyklus, kde se postaram o reseni problemu
            for (int i = problematicindexes.Count - 1; i >= 0; i--)
            {
                parts.RemoveAt(problematicindexes[i]);
            }


            return porizovaciCena * 0.5 + pomZb;
        }

    }
}
