﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Part
    {
        private int cena;
        public Part(int aCena)
        {
            cena = aCena;
        }

        public int GetZbytkovaCena()
        {
            if (cena < 0)
            {
                throw new ExceptionNegativePrice(cena, String.Format("Problem ve vypoctu zbytkove hodnoty, zaporna vstrupni cena {0}", cena));
            }
            else
            {
                return cena;
            }
        }
    }
}
