﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using kolekce;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private Student s = new Student("","",-25);

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                button1.Text = JePlnolety(s).ToString();
            }
            catch (ArgumentNullException e1)
            {
                button1.Text = "nelze zjistit";
            }
            catch (ArgumentOutOfRangeException e2)
            {
                button1.Text = e2.ActualValue.ToString() + "neni platny vek";
            }
            catch
            {
                //co delat v ostatnich pripadech
            }
            finally 
            {
                Cursor.Current = Cursors.Default;
            }                         
        }        

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                button1.Text = JePlnolety(s).ToString();
            }
            catch (ArgumentNullException e1)
            {
                DoSomeBusiness(e1, button1);
                throw;
            }                                    
            finally
            {
                Cursor.Current = Cursors.Default;
            }                         
        }

        private void DoSomeBusiness(ArgumentNullException e1, Button b)
        { }

        private bool JePlnolety(Student stud)
        {
            if (stud == null) {
                throw new ArgumentNullException("stud", "Parametr Student stud can not be null."); 
            }
            if (stud.Age < 0) {
                throw new ArgumentOutOfRangeException("stud", stud.Age, "Student can not have age less then 0");
            }
            return (stud.Age > 18);
        }

        
    }
}
