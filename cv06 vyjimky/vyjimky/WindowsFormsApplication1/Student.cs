﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kolekce
{
    public class Student
    {
        private string jmeno;
        private string prijmeni;

        public string Jmeno { get { return jmeno; } }
        public string Prijmeni { get { return prijmeni; } }
        public int Age { get; set; }
        
        public Student(string jmeno, string prijmeni, int vek)
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            Age = vek;
        }

        public override string ToString()
        {
            return String.Format("Student jmeno: {0} {1}", Prijmeni, Jmeno);
        }
        
    }
}
