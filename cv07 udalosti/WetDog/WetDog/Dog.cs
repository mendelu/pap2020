﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WetDog
{
    class Dog
    {
        private string name;

        private bool isOutside = false;

        public bool IsOutside
        {
            get { return isOutside; }
            set {
                bool pom = isOutside;
                isOutside = value;
                if ((isOutside) && (pom == false))
                {
                    //vyvolej udalost
                    DogEventArgs dea = new DogEventArgs();
                    dea.dogName = this.name;
                    OnDogWetted(dea);
                }
            }
        }

        public event EventHandler<DogEventArgs> DogWetted;

        protected virtual void OnDogWetted(DogEventArgs e)
        {
            Console.WriteLine("haf haf, pomoc prsi");
            DogWetted?.Invoke(this, e);
        }

        /*
         * protected virtual void OnDogWetted(EventArgs e)
        {
            EventHandler handler = DogWetted;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        */

        public Dog(string name)
        {
            this.name = name;
        }
    }
}
