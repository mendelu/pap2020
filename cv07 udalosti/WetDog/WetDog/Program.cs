﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WetDog
{
    class Program
    {
        static void Main(string[] args)
        {
            Greenpeace green = new Greenpeace();
            DogHotel hotel = new DogHotel();

            Dog dasenka = new Dog("Dasenka");
            dasenka.DogWetted += green.ConsumeDogWetted;
            dasenka.DogWetted += hotel.ConsumeDogWetted;

            dasenka.IsOutside = true;

            Console.ReadLine();
        }
    }
}
