﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolekce
{
    public class Lecture : IEquatable<Lecture>
    {
        private DateTime start;
        public DateTime Start { get { return start; } }

        private string classRoom;
        public string ClassRoom { get { return classRoom; } }

        public Lecture(DateTime kdy, string kde)
        {
            start = kdy;
            classRoom = kde;
        }

        public override string ToString()
        {
            return String.Format("lecture {0} at {1}h", start.DayOfWeek.ToString(), start.Hour.ToString());
        }

        public bool Equals(Lecture other)
        {
            return ((start.DayOfWeek == other.Start.DayOfWeek) &
                (start.Hour.Equals(other.Start.Hour))&
                (classRoom.Equals(other.ClassRoom)));
        }
    }
}
