﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolekce
{
    public struct Reservation : IComparer<Reservation>
    {
        private string jmeno;
        private string prijmeni;
        private string subjectName;
        private Lecture lect;

        public Reservation(string jmeno, string prijmeni, string subjectName, Lecture lecture)
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.subjectName = subjectName;
            this.lect = lecture;
        }

        public override string ToString()
        {
            return String.Format("reservace: student {0} {1}, predmet {2}, lecture {3}", jmeno, prijmeni, subjectName, lect);
        }

        public int Compare(Reservation x, Reservation y)
        {
            return (x.jmeno + x.prijmeni + x.subjectName).CompareTo(y.jmeno + y.prijmeni + y.subjectName);
        }
    }
}
