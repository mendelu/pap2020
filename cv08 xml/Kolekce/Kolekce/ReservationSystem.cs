﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolekce
{
    public class ReservationSystem
    {
        private SortedList<BaseInfo, Student> students;
        private Dictionary<string, Subject> subjects;
        private SortedDictionary<Reservation, Reservation> reservations;

        public ReservationSystem()
        {
            students = new SortedList<BaseInfo, Student>();
            subjects = new Dictionary<string, Subject>();
            reservations = new SortedDictionary<Reservation, Reservation>();
        }

        public bool AddSubject(string nameOfSubject)
        {
            if (subjects.ContainsKey(nameOfSubject)) return false;
            //key musi implementovat IEquatable<T>, v tomto pripade je splneno, neb klicem je string
            Subject newSubject = new Subject(nameOfSubject);
            subjects.Add(newSubject.Jmeno, newSubject);
            return true;
        }

        public Subject GetSubject(string nameOfSubject)
        {
            try
            {
                return subjects[nameOfSubject];
            }
            catch (KeyNotFoundException e)
            {
                return null;
            }
        }

        public bool AddStudent(Student newStudent)
        {
            try
            {
                students.Add(newStudent.Info, newStudent);
                //students.Add(newStudent.Info.Prijmeni+newStudent.Info.Jmeno, newStudent);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public void PrintStudents()
        {
            Console.WriteLine("Studenti v systemu: ");
            foreach (KeyValuePair<BaseInfo, Student> kv in students)
            {
                Console.WriteLine(kv.Value.ToString());
            }
        }

        public Student GetStudent(string jmeno, string prijmeni)
        {
            BaseInfo klic = new BaseInfo(jmeno, prijmeni);
            return students[klic];
            //return students[prijmeni + jmeno];
        }

        public bool RemoveStudent(string jmeno, string prijmeni)
        {
            BaseInfo klic = new BaseInfo(jmeno, prijmeni);
            return students.Remove(klic);
        }

        public Lecture AddLectureForSubject(string nameOfSubject, DateTime when, string classRoom)
        {
            if (subjects.ContainsKey(nameOfSubject) == false)
            {
                Console.WriteLine("nelze pridat rozvrhovou jednotku do neexistujiciho predmetu");
                return null;
            }

            Lecture newLecture = new Lecture(when, classRoom);
            if (subjects[nameOfSubject].ContainsLecture(newLecture))
            {
                Console.WriteLine("nelze pridat, rozvrhova jednotka jiz je k predmetu pridana");
                return null;
            }

            subjects[nameOfSubject].AddLecture(newLecture);
            return newLecture;
        }

        public bool CreateReservation(string jmeno, string prijmeni, string subjectName, Lecture lect)
        {
            Student who = GetStudent(jmeno, prijmeni);
            if (who == null) return false;

            Subject sub = GetSubject(subjectName);
            if (sub == null) return false;

            if (!sub.ContainsLecture(lect)) return false;

            Reservation r = new Reservation(jmeno, prijmeni, subjectName, lect);
            reservations.Add(r, r);
            return true;            
        }
    }
}
