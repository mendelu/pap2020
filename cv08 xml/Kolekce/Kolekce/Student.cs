﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolekce
{
    public struct BaseInfo : IComparable<BaseInfo>
    {
        private string jmeno;
        private string prijmeni;

        public string Jmeno { get { return jmeno; } }
        public string Prijmeni { get { return prijmeni; } }

        public BaseInfo(string jm, string prij)
        {
            jmeno = jm;
            prijmeni = prij;
        }

        public int CompareTo(BaseInfo other)
        {
            return (this.prijmeni + this.jmeno).CompareTo(other.prijmeni + other.jmeno);
        }
    }

    public class Student
    {
        private BaseInfo info;
        public BaseInfo Info { get { return info; } }

        public Student(string jmeno, string prijmeni)
        {
            info = new BaseInfo(jmeno, prijmeni);
        }

        public override string ToString()
        {
            return (info.Jmeno + " " + info.Prijmeni);
        }
    }
}
