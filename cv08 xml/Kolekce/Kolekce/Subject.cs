﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolekce
{
    public class Subject
    {
        string jmeno;
        public string Jmeno { get { return jmeno; } }

        private List<Lecture> lectures;

        public Subject(string name)
        {
            jmeno = name;
            lectures = new List<Lecture>(10);
        }

        public override string ToString()
        {
            string s = jmeno + "\n";
            foreach (var lec in lectures)
            {
                s += "    " + lec.ToString() + "\n";
            }
            return s;
        }

        public bool ContainsLecture(Lecture lec)
        {
            return lectures.Contains(lec);
        }

        public void AddLecture(Lecture newLecture)
        {
            if (!ContainsLecture(newLecture))
            {
                lectures.Add(newLecture);
            }
        }

        public void RemoveLecture(Lecture lecToDelete)
        {            
            lectures.Remove(lecToDelete);
        }

        public ReadOnlyCollection<Lecture> GetLecturesForRead()
        {
            return lectures.AsReadOnly();
        }


    }
}
