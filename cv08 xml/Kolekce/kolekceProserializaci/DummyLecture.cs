﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace kolekceProserializaci
{
    [Serializable]
    public class DummyLecture
    {
        public DateTime Start { get; set;}

        [XmlAttribute("CRoom")]
        public string ClassRoom { get; set; }

        public DummyLecture(DateTime kdy, string kde)
        {
            Start = kdy;
            ClassRoom = kde;
        }

        public DummyLecture()
        { }

    }
}
