﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace kolekceProserializaci
{
    [Serializable]
    public class DummyStudent
    {
        [XmlIgnore]
        public string ID { get; set; }
        
        public string Jmeno { get; set; }
        
        [XmlElement("Surname")]
        public string Prijmeni { get; set; }

        public DummyStudent()
        { }

        public DummyStudent(string jmeno, string prijmeni)
        {
            this.Jmeno = jmeno;
            this.Prijmeni = prijmeni;
        }
    }
}
