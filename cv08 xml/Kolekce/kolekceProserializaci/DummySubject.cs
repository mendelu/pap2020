﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace kolekceProserializaci
{
    [Serializable]
    [XmlRoot("SubjectRoot")]
    public class DummySubject
    {
        public string Jmeno { get; set; }

        [XmlArray("VyukoveJednotky"), XmlArrayItem(typeof(DummyLecture), ElementName = "Lecture")]
        public DummyLecture[] Lectures;

        public DummySubject(string name, List<DummyLecture> dlectures)
        {
            Jmeno = name;
            Lectures = dlectures.ToArray();
        }

        public DummySubject()
        { }
    }
}
