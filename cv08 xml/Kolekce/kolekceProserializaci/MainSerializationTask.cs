﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Kolekce;

namespace kolekceProserializaci
{
    class MainSerializationTask
    {
        public void SerializeDummyStudent()
        {
            DummyStudent dStudent = new DummyStudent("Jan", "Novak");
            XmlSerializer mySerializer = new XmlSerializer(typeof(DummyStudent));
            StreamWriter myWriter = new StreamWriter("filedummyStudent.xml");
            mySerializer.Serialize(myWriter, dStudent);
            myWriter.Close();
        }

        public void SerializeDummyStudentAlt()
        {
            DummyStudent dStudent = new DummyStudent("Jan", "Novak");
            XmlSerializer mySerializer = new XmlSerializer(typeof(DummyStudent));

            using (StreamWriter myWriter = new StreamWriter("filedummyStudent.xml"))
            {
                mySerializer.Serialize(myWriter, dStudent);
            }
        }

        public void DeserializeDummyStudent()
        {
            FileStream myFileStream = new FileStream("filedummyStudent.xml", FileMode.Open);

            XmlSerializer mySerializer = new XmlSerializer(typeof(DummyStudent));
            DummyStudent newone = (DummyStudent)mySerializer.Deserialize(myFileStream);
            Console.WriteLine(" {0} {1} {2}", newone.Jmeno, newone.Prijmeni, newone.ID);

            myFileStream.Close();
        }

        public void SerializeDummySubject()
        {
            DummyLecture d1 = new DummyLecture(new DateTime(2020, 11, 24, 7, 0, 0), "online");
            DummyLecture d2 = new DummyLecture(new DateTime(2020, 11, 24, 11, 0, 0), "online");
            List<DummyLecture> listofDL = new List<DummyLecture>();
            listofDL.Add(d1);
            listofDL.Add(d2);
            DummySubject ds = new DummySubject("PAP", listofDL);

            XmlSerializer mySerializer = new XmlSerializer(typeof(DummySubject));

            using (StreamWriter myWriter = new StreamWriter("filedummySubject.xml"))
            {
                mySerializer.Serialize(myWriter, ds);
            }
        }

        public void DeserializeDummySubject()
        {
            FileStream myFileStream = new FileStream("filedummySubject.xml", FileMode.Open);

            XmlSerializer mySerializer = new XmlSerializer(typeof(DummySubject));
            DummySubject newone = (DummySubject)mySerializer.Deserialize(myFileStream);
            Console.WriteLine("{0} {1}", newone.Lectures[0].Start, newone.Lectures[0].ClassRoom);

            myFileStream.Close();
        }

        public void SerializeLecture()
        {
            Lecture l1 = new Lecture(new DateTime(2020, 11, 24, 23, 0, 0), "Q05");

            FileStream myFileStream = new FileStream("filedLecture.xml", FileMode.Create);

            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(myFileStream, l1);
            myFileStream.Close();
        }

        public void DeserializeLecture()
        {
            FileStream myFileStream = new FileStream("filedLecture.xml", FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            Lecture l1 = (Lecture)bf.Deserialize(myFileStream);
            myFileStream.Close();
            Console.WriteLine(l1);
        }
    }
}
