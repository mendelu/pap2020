﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kolekceProserializaci
{
    class Program
    {
        static void Main(string[] args)
        {
            MainSerializationTask mst = new MainSerializationTask();
            mst.SerializeDummyStudent();

            mst.DeserializeDummyStudent();

            mst.SerializeDummySubject();
            mst.DeserializeDummySubject();

            mst.SerializeLecture();
            mst.DeserializeLecture();
            Console.ReadLine();
        }
    }
}
