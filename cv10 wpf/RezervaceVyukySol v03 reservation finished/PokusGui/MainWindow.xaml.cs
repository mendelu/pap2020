﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using kolekce;

namespace PokusGui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ReservationSystem rs;
        WindowStudent ws;

        public MainWindow()
        {
            InitializeComponent();
            SetCal();
            rs = new ReservationSystem();
            //MainGrid.DataContext = rs;
            listBoxStudents.ItemsSource = rs.students;

            ws = new WindowStudent();
        }

        private void SetCal()
        {
            Cal1.DisplayDateStart = new DateTime(2020, 12, 1);
            Cal1.DisplayMode = CalendarMode.Month;
            Cal1.BlackoutDates.Add(new CalendarDateRange(new DateTime(2020, 12, 5), new DateTime(2020, 12, 6)));

            for (int i=7; i<20; i++)
            {
                comboBoxHours.Items.Add(i);
            }
            
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            rs.AddStudent(textBoxJmeno.Text, textBoxPrijmeni.Text);
            //listBoxStudents.Items.Add(rs.GetStudent(textBoxJmeno.Text, textBoxPrijmeni.Text));
            

            //ws.rs = this.rs;
            //ws.ShowDialog();
            

        }

        private void buttonAddSubject_Click(object sender, RoutedEventArgs e)
        {
            rs.AddSubject(textBoxSubject.Text);
            listBoxSubjects.Items.Add(rs.GetSubject(textBoxSubject.Text));
        }

        private void buttonLecture_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxSubjects.SelectedItem == null)
            {
                MessageBox.Show("vyberte Subject", "warning", MessageBoxButton.OK);
                return;
            }

            if (Cal1.SelectedDate == null)
            {
                MessageBox.Show("vyberte Datum", "warning", MessageBoxButton.OK);
                return;
            }

            string sname = ((Subject)listBoxSubjects.SelectedItem).Name;
            DateTime start = Cal1.SelectedDate.Value;
            double h = Convert.ToDouble(comboBoxHours.SelectedItem);
            start.AddHours(h);
            rs.AddLectureForSubject(sname, start, textBoxRoom.Text);

            listBoxSubjects_SelectionChanged(this, null);
        }

        private void listBoxSubjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBoxSubjects.SelectedItem == null)
            {
                return;
            }
            Subject vybrany = (Subject)listBoxSubjects.SelectedItem;
            listBoxLectures.ItemsSource = vybrany.LecturesReadOnly;
        }

        private void ButtonAddRes_Click(object sender, RoutedEventArgs e)
        {
            if ((listBoxStudents.SelectedItem == null) | (listBoxSubjects.SelectedItem == null) | (listBoxLectures.SelectedItem == null))
            {
                MessageBox.Show("nejsou vybrany potrebne hodnoty", "registrace nevytvorena", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            //Student who = ((KeyValuePair<BaseInfo, Student>)listBoxStudents.SelectedItem).Value;
            Student who = (Student)listBoxStudents.SelectedItem;

            Subject what = listBoxSubjects.SelectedItem as Subject;
            Lecture which = listBoxLectures.SelectedItem as Lecture;

            Reservation newRes = new Reservation();
            rs.CreateReservation(who.Info.Jmeno, who.Info.Prijmeni, what.Name, which, out newRes);
            if (rs != null)
            {
                listBoxReservations.Items.Add(newRes);
            }
        }
    }
}
