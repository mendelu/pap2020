﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using kolekce;

namespace PokusGui
{
    /// <summary>
    /// Interaction logic for WindowStudent.xaml
    /// </summary>
    public partial class WindowStudent : Window
    {
        public ReservationSystem rs;

        public WindowStudent()
        {
            InitializeComponent();
        }

        private void buttonAddStudent_Click(object sender, RoutedEventArgs e)
        {
            rs.AddStudent(textBoxJmeno.Text, textBoxPrijmeni.Text);
            //Close();
            Hide();
        }
    }
}
