﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace kolekce
{
    /// <summary>
    /// Jednoznačný identifikátor je den v týdnu, hodina začátku a místo konání
    /// </summary>
    public class Lecture : IEquatable<Lecture>
    {
        private DateTime start;
        public DateTime Start { get { return start; } }

        private string classRoom;
        public string ClassRoom { get { return classRoom; } }

        public Lecture(DateTime start, string classRoom)
        {
            this.start = start;
            this.classRoom = classRoom;
        }
        
        public override string ToString()
        {
            string when = start.DayOfWeek.ToString() + " " + start.Hour.ToString() + "h";
            return String.Format("lecture: at {0} on {1}", classRoom, when);
        }
        

        
        /*
        public override string ToString()
        {
            string den = start.ToString("dddd"); //dlouhy nazev dne, dle formátu nastaveném v ovládacím panelu  Jazyk - Nastavení formátu času
            /* lze použít d, dd, ddd, dddd --> 1, 01, Po, Pondělí
             * dále např.
             * h H pro hodinu 0-12 resp 0-24
             * m minuty
             * M MM MMM MMMM měsíc
             * yyyy rok 
             * atd..viz: https://msdn.microsoft.com/en-us/library/8kb3ddd4%28v=vs.110%29.aspx             
             

            //pripadne lze pouzit konkretni kulturu, trida CultureInfo splnuje rozhrani IFormatProvider
            CultureInfo ci = new CultureInfo("fr-FR"); //rustina ru-RU ...musi se ale vyresit problem se znakovou sadou konzole Console.OutputEncoding = System.Text.Encoding.UTF8;  
                                                       //tahistina "th-TH" tady nam nebude stavit ani UTF8
                                                       // https://msdn.microsoft.com/en-us/library/8tfzyc64%28v=vs.110%29.aspx            
            den = start.ToString("dddd", ci);            
            return den;
        }*/


        public bool Equals(Lecture other)
        {
            return ((start.DayOfWeek == other.Start.DayOfWeek) & (start.Hour == other.Start.Hour) & (classRoom == other.ClassRoom));
        }
    }
}
