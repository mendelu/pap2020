﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace kolekce
{
    public class ObservableSortedList : SortedList<BaseInfo, Student>, INotifyCollectionChanged
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public new bool Add(BaseInfo baseInfo, Student student)
        {
            if (this.ContainsKey(baseInfo)) { return false; }
            base.Add(baseInfo, student);
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            return true;
        }

        public new bool Remove(BaseInfo baseinfo)
        {
            bool odebrano = base.Remove(baseinfo);
            if (odebrano)
            {
                CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                return true;
            }
            return false;
        }
    }
}
