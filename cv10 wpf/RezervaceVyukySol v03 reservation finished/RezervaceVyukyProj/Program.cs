﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using kolekce;

namespace kolekce
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;            

            Lecture l1 = new Lecture(new DateTime(2014, 11, 10, 8, 0, 0), "Q05");
            Lecture l2 = new Lecture(new DateTime(2014, 11, 11, 8, 0, 0), "Q05");
            Lecture l3 = new Lecture(new DateTime(2014, 11, 12, 8, 0, 0), "Q05");
            Lecture l4 = new Lecture(new DateTime(2014, 11, 13, 8, 0, 0), "Q05");
            Console.WriteLine(l1.ToString());
            Console.WriteLine("------------");

            Subject sub1 = new Subject("PAP");
            sub1.AddLecture(l1);
            sub1.AddLecture(l2);
            sub1.AddLecture(l3);
            sub1.AddLecture(l4);

            //Console.WriteLine(sub1.ToString());

            bool found = sub1.containsLecture((new Lecture(new DateTime(2014, 11, 13, 8, 0, 0), "Q05")));

            ReservationSystem rs = new ReservationSystem();
            rs.AddStudent("David", "Davidovsky");
            rs.AddStudent("Adela", "Oprsalkova");
            rs.AddStudent("Petr", "Davidovsky");                 
            rs.PrintStudents();

            Student foundS = rs.GetStudent("Petr", "Davidovsky");
            //if (foundS != null) Console.WriteLine(foundS);

            rs.AddSubject("PAP");
            rs.AddSubject("JAVA");

            rs.AddLectureForSubject("PAP", new DateTime(2014, 11, 10, 8, 0, 0), "Q05");
            rs.AddLectureForSubject("PAP", new DateTime(2014, 11, 11, 8, 0, 0), "Q05");
            rs.AddLectureForSubject("PAP", new DateTime(2014, 11, 12, 8, 0, 0), "Q05");

            rs.AddLectureForSubject("JAVA", new DateTime(2014, 11, 10, 9, 0, 0), "Q05");
            rs.AddLectureForSubject("JAVA", new DateTime(2014, 11, 11, 9, 0, 0), "Q05");
            rs.AddLectureForSubject("JAVA", new DateTime(2014, 11, 12, 9, 0, 0), "Q05");

            Reservation r1;
            rs.CreateReservation("David", "Davidovsky", "PAP", new Lecture(new DateTime(2014, 11, 10, 8, 0, 0), "Q05"), out r1);            

            Console.ReadLine();

        }
    }
}
