﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kolekce
{
    public struct Reservation : IComparable<Reservation>
    {
        private string jmeno;
        private string prijmeni;
        private string subjectName;
        private Lecture lect;

        public Reservation(string jmeno, string prijmeni, string subjectName, Lecture lecture)
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
            this.subjectName = subjectName;
            this.lect = lecture;
        }

        public override string ToString()
        {
            return String.Format("reservace: student {0} {1}, predmet {2}, cviceni {3}", prijmeni, jmeno, subjectName, lect);
        }

        public string Caption
        {
            get { return String.Format("reservace na jmeno {0}", prijmeni); }
        }

        public int CompareTo(Reservation other)
        {                        
            string s1 = jmeno + prijmeni + subjectName;
            string s2 = other.jmeno + other.prijmeni + other.subjectName;
            return s2.CompareTo(s1);
        }
    }
    
}
