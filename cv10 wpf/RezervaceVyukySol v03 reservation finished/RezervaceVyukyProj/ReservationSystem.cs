﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kolekce
{    

    public class ReservationSystem
    {
        public ObservableSortedList students = new ObservableSortedList(); 
        //private SortedList<BaseInfo, Student> students = new SortedList<BaseInfo, Student>();
        private Dictionary<string, Subject> subjects = new Dictionary<string, Subject>(); //nazev predmetu, predmet
        private SortedDictionary<Reservation, Reservation> reservations = new SortedDictionary<Reservation, Reservation>();

        public IList<Student> Students { get { return students.Values; } }


        public bool AddStudent(string jmeno, string prijmeni)
        {
            Student newStudent = new Student(jmeno, prijmeni);
            //test klice
            if (students.ContainsKey(newStudent.Info)) return false;
            students.Add(newStudent.Info, newStudent);
            return true;
        }

        public void PrintStudents()
        {
            foreach (KeyValuePair<BaseInfo,Student> kvp in students)
            {
                Console.WriteLine( kvp.Value.ToString() );
            }
            //  alternativa
            /*
            foreach (Student s in students.Values)
            {
                Console.WriteLine(s.ToString());
            }
             */             
        }

        public Student GetStudent(string jmeno, string prijmeni)
        {           
            BaseInfo info = new BaseInfo(jmeno,prijmeni);
            return students[info];
        }

        public bool RemoveStudent(string prijmeni, string jmeno)
        {
            BaseInfo info = new BaseInfo(jmeno,prijmeni);
            return students.Remove(info);
        }


        public bool AddSubject(string nameOfSubject)
        {
            if (subjects.ContainsKey(nameOfSubject)) return false;
            //key musi implementovat IEquatable<T>, v tomto pripade je splneno, neb klicem je string
            Subject newSubject = new Subject(nameOfSubject);
            subjects.Add(newSubject.Name, newSubject);
            return true;
        }

        public Subject GetSubject(string subjectName)
        {
            return subjects[subjectName];
        }

        public Lecture AddLectureForSubject(string subjectName, DateTime start, string classRoom)
        {
            if (subjects.ContainsKey(subjectName) == false)
            {
                Console.WriteLine("nelze pridat rozvrhovou jednotku do neexistujiciho predmetu");
                return null;
            }

            Lecture newLecture = new Lecture(start, classRoom);
            if (subjects[subjectName].containsLecture(newLecture)) 
            {
                Console.WriteLine("nelze pridat, rozvrhova jednotka jiz je k predmetu pridana");
                return null;
            }

            subjects[subjectName].AddLecture(newLecture);
            return newLecture;
        }


        public bool CreateReservation(string jmeno, string prijmeni, string subjectName, Lecture lecture, out Reservation newReservation)
        {
            newReservation = new Reservation();
            Student who = GetStudent(jmeno, prijmeni);
            if (who == null) return false; //student neexistuje;

            Subject subj = GetSubject(subjectName);
            if (subj == null) return false; //neexistuje predmet

            if (subj.containsLecture(lecture) == false) return false; //predmet nema konkretni lecture;

            Reservation r = new Reservation(jmeno, prijmeni, subjectName, lecture);                                    
            if (reservations.ContainsKey(r)) return false; //rezervace uz existuje;

            reservations.Add(r, r);
            newReservation = r;
            return true;
        }



    }
}
