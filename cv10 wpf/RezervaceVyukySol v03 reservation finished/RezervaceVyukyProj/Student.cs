﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kolekce
{
    //public struct StudentBaseInfo : IComparer<StudentBaseInfo> //kvuli hastable, resp. dictionary<Tk,Tv>

    public struct BaseInfo : IComparable<BaseInfo> 
    {
        
        private string jmeno;
        private string prijmeni;
        
        public string Jmeno {get{return jmeno;}}
        public string Prijmeni {get{return prijmeni;}}
        
        public BaseInfo(string jmeno, string prijmeni)
        {
            this.jmeno = jmeno;
            this.prijmeni = prijmeni;
        }        
        
        public int CompareTo(BaseInfo other)
        {
            string complex = prijmeni + jmeno;  
            return complex.CompareTo(other.Prijmeni+other.Jmeno);
        }
        
    }

    /// <summary>
    /// Jednoznacnym identifikatorem je jmeno + prijmeni
    /// </summary>
    public class Student
    {
        private BaseInfo info;

        public BaseInfo Info { get { return info; }
        }
                        
        public Student(string jmeno, string prijmeni)
        {            
            info = new BaseInfo(jmeno, prijmeni);
        }

        public override string ToString()
        {
            return String.Format("Student jmeno: {0} {1}", info.Prijmeni, info.Jmeno);
        }
        
    }
}
