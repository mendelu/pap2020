﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kolekce
{
    /// <summary>
    /// jednoznačným identifikátorem je name
    /// </summary>
    public class Subject
    {
        private string name;
        public string Name { get { return name; } }

        private List<Lecture> lectures;
        //public List<Lecture> Lectures { get { return lectures; } } //zverejni i pro zapis, kolekce by sla modifikovat

        //zverejneni pro cteni
        public IList<Lecture> LecturesReadOnly { get { return lectures.AsReadOnly(); } } 

        //alternativa
        public IEnumerable<Lecture> Lectures { get { return lectures.AsEnumerable<Lecture>(); } }
                    
        public bool containsLecture(Lecture lectureToFind)
        {
            return lectures.Contains(lectureToFind); //vyzaduje IEquatable u Lecture
        }

        public Subject(string name)
        {
            this.name = name;
            lectures = new List<Lecture>();
        }

        public void AddLecture(Lecture anotherLecture)
        {
            lectures.Add(anotherLecture);
        }

        public bool RemoveLecture(Lecture lectureToDelete)
        {
            return lectures.Remove(lectureToDelete); //vyzaduje implementaci IEquatable<T>
        }

        public override string ToString()
        {
            string s = name + "\n";
            foreach (Lecture lec in lectures)
            {
                s += "   " + lec.ToString() + "\n";
            }
            return s;
        }

    }
}
